# TP2 - Réseau, On va router des trucs

## I. ARP
### 1. Echange ARP

sur la VM1 :

commande : `ping 10.2.1.12`

resultat : 
```
[leo@node1 ~]$ ping 10.2.1.12
PING 10.2.1.12 (10.2.1.12) 56(84) bytes of data.
64 bytes from 10.2.1.12: icmp_seq=1 ttl=64 time=0.823 ms
64 bytes from 10.2.1.12: icmp_seq=2 ttl=64 time=0.535 ms
64 bytes from 10.2.1.12: icmp_seq=3 ttl=64 time=0.395 ms
64 bytes from 10.2.1.12: icmp_seq=4 ttl=64 time=0.384 ms
--- 10.2.1.12 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3081ms
rtt min/avg/max/mdev = 0.384/0.534/0.823/0.177 ms
```

sur la VM2 :

commande : `ping 10.2.1.11`

resultat : 
```
[leo@node2 ~]$ ping 10.2.1.11
PING 10.2.1.11 (10.2.1.11) 56(84) bytes of data.
64 bytes from 10.2.1.11: icmp_seq=1 ttl=64 time=0.514 ms
64 bytes from 10.2.1.11: icmp_seq=2 ttl=64 time=0.334 ms
64 bytes from 10.2.1.11: icmp_seq=3 ttl=64 time=0.316 ms
64 bytes from 10.2.1.11: icmp_seq=4 ttl=64 time=0.422 ms
--- 10.2.1.11 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3060ms
rtt min/avg/max/mdev = 0.316/0.396/0.514/0.081 ms
```


Table ARP VM 1 :

commande : `ip neigh show`

resultat : 
```
[leo@node1 ~]$ ip neigh show
10.2.1.12 dev enp0s8 lladdr 08:00:27:9c:24:d7 STALE
10.2.1.1 dev enp0s8 lladdr 0a:00:27:00:00:09 REACHABLE
10.0.2.2 dev enp0s3 lladdr 52:54:00:12:35:02 STALE
```
on peut voir que dans cette table, l'adresse MAC de la VM 2 est :`08:00:27:9c:24:d7`

Table ARP VM 2 :

commande : `ip neigh show`

resultat : 
```
[leo@node2 ~]$ ip neigh show
10.2.1.11 dev enp0s8 lladdr 08:00:27:0c:18:d0 STALE
10.2.1.1 dev enp0s8 lladdr 0a:00:27:00:00:09 DELAY
```
on peut voir que dans cette table, l'adresse MAC de la VM 1 est :`08:00:27:0c:18:d0`

avec la commande `ip a` je recupère les adresses MAC des VM 1 et 2 : 

resultat :
VM 1 :
```
enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:0c:18:d0 brd ff:ff:ff:ff:ff:ff
    inet 10.2.1.11/24 brd 10.2.1.255 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe0c:18d0/64 scope link
       valid_lft forever preferred_lft forever
```

VM 2 :
```
enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:9c:24:d7 brd ff:ff:ff:ff:ff:ff
    inet 10.2.1.12/24 brd 10.2.1.255 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe9c:24d7/64 scope link
       valid_lft forever preferred_lft forever
```

on peut remarquer que l'adresse MAC de la VM 1 (`08:00:27:0c:18:d0`) et l'adresse MAC de la VM 2 (`08:00:27:9c:24:d7`) trouvées avec `ip a` sont biens les memes que dans les tables ARP.

### 2. Analyse de trames

je lance une capture de trames avec la commande : `sudo tcpdump -i enp0s8 -w captures2.pcap`

je vide les tables ARP des deux VM avec la commande : `sudo ip neigh flush all`

je ping ma VM 2 depuis la VM 1 avec la commande : 
`ping 10.2.1.12`

listes des requetes obtenues dans Wireshark :

| ordre | type trame  | source                      | destination                   |
|-------|-------------|-----------------------------|-------------------------------|
| 1     | Requête ARP | `node1` `08:00:27:0C:18:D0` | Broadcast `FF:FF:FF:FF:FF:FF` |
| 2     | Réponse ARP | `node2` `0A:00:27:00:00:09` | `node1`   `08:00:27:0C:18:0D` |
| 3     | Réponse ARP | `node2` `08:00:27:9c:24:D7` | Broadcast `FF:FF:FF:FF:FF:FF` |
| 4     | Réponse ARP | `node1` `08:00:27:0C:18:D0` | Broadcast `FF:FF:FF:FF:FF:FF` |
| 5     | Réponse ARP | `node2` `08:00:27:9C:24:D7` | `node1`   `08:00:27:9C:24:D7` |


## II. Routage
### 1. Mise en place du routage

je créer 3 VM avec comme hostname : `node1.net1.tp2`, `marcel.net2.tp2` et `router.net2.tp2`
je modifie le fichier : `/etc/sysconfig/network-scripts/ifcfg-enp0s8` pour leur attribuer des IP fixe 
et je configure 2 interfaces sur la VM router pour qu'elle puisse joindre les deux autres machines.

sur node1, je modifie le fichier `/etc/sysconfig/network-scripts/route-enp0s8` pour ajouter la ligne : 
`10.2.2.12/24 via 10.2.2.254 dev enp0s8` et sur marcel la ligne : `10.2.1.11/24 via 10.2.2.254 dev enp0s8`

et sur la vm router j'active le routage avec les commandes : 
```
sudo firewall-cmd --list-all
sudo firewall-cmd --get-active-zone
sudo firewall-cmd --add-masquerade --zone=public
sudo firewall-cmd --add-masquerade --zone=public --permanent

```

j'applique les mises a jour avec les commandes : `nmcli con reload` et `nmcli con up enp0s8` sur les deux VM.

puis je teste un ping de node1 vers marcel 
resultat :
```
[leo@node1 ~]$ ping 10.2.2.12
PING 10.2.2.12 (10.2.2.12) 56(84) bytes of data.
64 bytes from 10.2.2.12: icmp_seq=1 ttl=63 time=0.588 ms
64 bytes from 10.2.2.12: icmp_seq=2 ttl=63 time=0.674 ms
64 bytes from 10.2.2.12: icmp_seq=3 ttl=63 time=0.685 ms
64 bytes from 10.2.2.12: icmp_seq=4 ttl=63 time=0.695 ms
--- 10.2.2.12 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3102ms
rtt min/avg/max/mdev = 0.588/0.660/0.695/0.049 ms
```

### 2. Analyse de trames


je vide les tables ARP des trois VM avec la commande : `sudo ip neigh flush all`

je ping marcel depuis la VM 1 avec la commande : `ping 10.2.2.12`

j'uilise la commande : `ip neigh show` pour afficher les tables ARP :

node1 :
```
[leo@node1 ~]$ ip neigh show
10.2.1.1 dev enp0s8 lladdr 0a:00:27:00:00:09 DELAY
10.2.1.254 dev enp0s8 lladdr 08:00:27:f5:14:dc STALE
```

marcel :
```
[leo@marcel ~]$ ip neigh show
10.2.2.1 dev enp0s8 lladdr 0a:00:27:00:00:07 DELAY
10.2.2.254 dev enp0s8 lladdr 08:00:27:d2:50:22 STALE
```

router :
```
[leo@routeur ~]$ ip neigh show
10.2.2.12 dev enp0s9 lladdr 08:00:27:9c:19:9f STALE
10.2.1.1 dev enp0s8 lladdr 0a:00:27:00:00:09 DELAY
10.2.1.11 dev enp0s8 lladdr 08:00:27:0c:18:d0 STALE
```
on peut constater que node1 et marcel sont assez similaire, ils ont tous les deux l'adresse IP et l'adresse MAC des host only adapter 
et le routeur lui contient l'adresse IP et MAC de node1 et marcel ainsi que l'IP et la MAC d'un des host only adapter. 

je lance une capture de trames avec la commande : `sudo tcpdump -i enp0s8 -w captures.pcap` sur node1 et marcel

je vide les tables ARP des trois VM avec la commande : `sudo ip neigh flush all`

puis je ping marcel avec node1 

avec la commande : `sudo firewall-cmd --add-port=1050/tcp` j'ouvre un port et je lance un serveur en python avec la commande : `python3 -m http.server 1050`
je recupère les captures depuis un navigateur web a l'adresse : `http://10.2.1.11:1050` pour node1 et `http://10.2.2.12:1050` pour marcel

listes des requetes obtenues dans Wireshark :

observation dans Wireshark pour node1 :
| ordre | type trame  | IP source | MAC source                   | IP destination | MAC destination              |
|-------|-------------|-----------|------------------------------|----------------|------------------------------|
| 1     | Requête ARP | 10.2.1.1  | host-only `0A:00:27:00:00:09`| 10.2.1.11      | node1     `08:00:27:0C:18:0D`|
| 2     | Réponse ARP | 10.2.1.11 | node1     `08:00:27:0C:18:0D`| 10.2.1.1       | host-only `0A:00:27:00:00:09`|
| 3     | Requête ARP | 10.2.1.254| marcel    `08:00:27:F5:14:DC`| 10.2.1.1       | broadcast `00:00:00:00:00:00`|
| 4     | Requête ARP | 10.2.1.11 | node1     `08:00:27:0C:18:D0`| 10.2.1.1       | broadcast `00:00:00:00:00:00`|
| 6     | Réponse ARP | 10.2.1.1  | host-only `0A:00:27:00:00:09`| 10.2.1.11      | node1     `08:00:27:0C:18:0D`|
| 7     | Requête ARP | 10.2.1.11 | node1     `08:00:27:0C:18:D0`| 10.2.1.11      | broadcast `00:00:00:00:00:00`|
| 8     | Réponse ARP | 10.2.1.254| marcel    `08:00:27:F5:14:DC`| 10.2.1.11      | node1     `08:00:27:0C:18:0D`|
| 9     | Requête ARP | 10.2.1.254| marcel    `08:00:27:F5:14:DC`| 10.2.1.11      | broadcast `00:00:00:00:00:00`|
| 10    | Réponse ARP | 10.2.1.254| node1     `08:00:27:0C:18:D0`| 10.2.1.254     | router    `08:00:27:F5:14:DC`|


observation dans Wireshark pour marcel :
| ordre | type trame  | IP source | MAC source                   | IP destination | MAC destination              |
|-------|-------------|-----------|------------------------------|----------------|------------------------------|
| 1     | Requête ARP | 10.2.2.1  | host-only `0A:00:27:00:00:07`| 10.2.2.12      | marcel    `08:00:27:9C:19:9F`|
| 2     | Réponse ARP | 10.2.2.12 | marcel    `08:00:27:9C:19:9F`| 10.2.2.1       | host-only `0A:00:27:00:00:07`|
| 3     | Requête ARP | 10.2.2.12 | marcel    `08:00:27:9C:19:9F`| 10.2.2.1       | broadcast `00:00:00:00:00:00`|
| 4     | Réponse ARP | 10.2.2.1  | node1     `0A:00:27:00:00:07`| 10.2.2.12      | marcel    `08:00:27:9C:19:9F`|
| 5     | Requête ARP | 10.2.2.254| router    `08:00:27:D2:50:22`| 10.2.2.12      | broadcast `00:00:00:00:00:00`|
| 6     | Réponse ARP | 10.2.2.12 | node1     `08:00:27:0C:18:D0`| 10.2.2.254     | router    `08:00:27:D2:50:22`|
| 7     | Requête ARP | 10.2.2.12 | marcel    `08:00:27:9C:19:9F`| 10.2.2.254     | broadcast `00:00:00:00:00:00`|
| 8     | Réponse ARP | 10.2.2.254| router    `08:00:27:D2:50:22`| 10.2.2.12      | marcel    `08:00:27:9C:19:9F`|


### 3. Accès internet

ajout d'une route par default sur node1 et marcel : 

node1 : 
commande : `sudo ip route add default via 10.2.1.254 dev enp0s8` 
et ajout definitif dans le fichier : `/etc/sysconfig/network` avec la ligne : `GATEWAY=10.2.1.254` 

marcel :
commande : `sudo ip route add default via 10.2.2.254 dev enp0s8` 
et ajout definitif dans le fichier : `/etc/sysconfig/network` avec la ligne : `GATEWAY=10.2.2.254`

et ensuite on reload les fichiers avec les commandes : `nmcli con reload` et `nmcli con up enp0s8`

je lance une capture de trames avec la commande : `sudo tcpdump -i enp0s8 -w captures.pcap` sur node1

je vide les tables ARP de node1 et du router avec la commande : `sudo ip neigh flush all`

puis je ping `8.8.8.8` 

je lance un serveur avec python et je recupère le fichier WireShark.
listes des requetes obtenues dans Wireshark :

| ordre | type trame | IP source   | MAC source                    | IP destination | MAC destination              |
|-------|------------|-------------|-------------------------------|----------------|------------------------------|
| 1     | ping       | `10.2.1.11` | node1     `08:00:27:0C:18:D0` | `10.2.1.1`     | broadcast `00:00:00:00:00:00`|
| 2     | pong       | `10.2.1.1`  | host-only `0A:00:27:00:00:09` | `10.2.1.11`    | node1     `08:00:27:0C:18:D0`|
| 2     | ping       | `10.2.1.254`| router    `08:00:27:F5:14:DC` | `10.2.1.1`     | broadcast `00:00:00:00:00:00`|
| 2     | ping       | `10.2.1.11` | node1     `08:00:27:0C:18:D0` | `10.2.1.254`   | broadcast `00:00:00:00:00:00`|
| 2     | pong       | `10.2.1.254`| router    `08:00:27:F5:14:DC` | `10.2.1.11`    | node1     `08:00:27:0C:18:D0`| 


## III. DHCP
### 1. Mise en place du serveur DHCP

mon linux a refusé d'installer le server quand j'ai utilisé la commande : `sudo dnf -y install dhcp-server` et je n'ai pas eu le temps 
de me pencher sur le problème. 

![](https://media.giphy.com/media/lKWlXRBGltz2g/giphy.gif?cid=ecf05e471dkmtj97681iwdm5f7u6dlgnw97tdl69yl2se83j&rid=giphy.gif&ct=g)
