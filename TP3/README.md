# TP3 : Progressons vers le réseau d'infrastructure

## I. (mini)Architecture réseau
### 1. Adressage


tableau d'adressage:

| Nom       | Adresse du réseau | Masque           | Nombre de clients possibles | Adresse passerelle | Adresse broadcast |
|-----------|-------------------|------------------|-----------------------------|--------------------|--------------------
| `client1` | `10.3.0.128`      | `255.255.255.192`| 62                          | `10.3.0.190`       | `10.33.0.191`     |
| `server1` | `10.3.0.0`        | `255.255.255.128`| 126                         | `10.3.0.126`       | `10.33.0.127`     |
| `server2` | `10.3.0.192`      | `255.255.255.192`| 14                          | `10.3.0.206`       | `10.33.0.207`     |

tableau des machines :

| Nom machine  | Adresse IP `client1` | Adresse IP `server1` | Adresse IP `server2` | Adresse de passerelle |
|--------------|----------------------|----------------------|----------------------|-----------------------|
| `router.tp3` | `10.3.0.190/26`      | `10.3.0.126/25`      | `10.3.0.206/28`      | Carte NAT             |
| ` `          | ...                  | ...                  | ...                  | `10.3.?.?/?`          |

### 2. Routeur

creation de la vm `routeur.tp3` : 

il a bien une ip dans chaque réseau :
```
[leo@router ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:e7:1d:1c brd ff:ff:ff:ff:ff:ff
    inet 10.0.2.15/24 brd 10.0.2.255 scope global dynamic noprefixroute enp0s3
       valid_lft 79769sec preferred_lft 79769sec
    inet6 fe80::a00:27ff:fee7:1d1c/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:47:da:d2 brd ff:ff:ff:ff:ff:ff
    inet 10.3.0.190/26 brd 10.3.0.191 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe47:dad2/64 scope link
       valid_lft forever preferred_lft forever
4: enp0s9: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:59:e0:6c brd ff:ff:ff:ff:ff:ff
    inet 10.3.0.126/25 brd 10.3.0.127 scope global noprefixroute enp0s9
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe59:e06c/64 scope link
       valid_lft forever preferred_lft forever
5: enp0s10: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:fb:d9:35 brd ff:ff:ff:ff:ff:ff
    inet 10.3.0.206/28 brd 10.3.0.207 scope global noprefixroute enp0s10
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fefb:d935/64 scope link
       valid_lft forever preferred_lft forever
```

il à acces a internet :
```
[leo@router ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=114 time=80.10 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=114 time=79.6 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=114 time=40.2 ms
64 bytes from 8.8.8.8: icmp_seq=4 ttl=114 time=29.5 ms
--- 8.8.8.8 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3005ms
rtt min/avg/max/mdev = 29.545/57.581/80.957/23.008 ms
```

il peut resoudre des noms de domaine :
```
[leo@router ~]$ ping google.com
PING google.com (142.250.179.110) 56(84) bytes of data.
64 bytes from par21s20-in-f14.1e100.net (142.250.179.110): icmp_seq=1 ttl=114 time=67.2 ms
64 bytes from par21s20-in-f14.1e100.net (142.250.179.110): icmp_seq=2 ttl=114 time=39.8 ms
64 bytes from par21s20-in-f14.1e100.net (142.250.179.110): icmp_seq=3 ttl=114 time=61.2 ms
64 bytes from par21s20-in-f14.1e100.net (142.250.179.110): icmp_seq=4 ttl=114 time=21.3 ms
--- google.com ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3002ms
rtt min/avg/max/mdev = 21.290/47.400/67.227/18.191 ms
```

il porte bien le bon nom : 
```
[leo@router ~]$ hostname
router.tp3
```

activation du routage pour cette VM : 
```
$ sudo firewall-cmd --list-all
$ sudo firewall-cmd --get-active-zone
$ sudo firewall-cmd --add-masquerade --zone=public
$ sudo firewall-cmd --add-masquerade --zone=public --permanent
```

## II. Services d'infra

### 1. Serveur DHCP

creation du dhcp : 

elle porte bien le bon nom : 
```
[leo@client1 ~]$ sudo echo 'dhcp.client1.tp3' |sudo tee /etc/hostname
dhcp.client1.tp3
```

il a acces a internet : 
```
[leo@client1 ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=113 time=48.10 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=113 time=23.7 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=113 time=56.5 ms
64 bytes from 8.8.8.8: icmp_seq=4 ttl=113 time=40.5 ms
--- 8.8.8.8 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3005ms
rtt min/avg/max/mdev = 23.694/42.415/56.538/12.212 ms
```

installation du server dhcp avec la commande : `sudo dnf install dhcp-server`

configuration du dhcp dans le fichier `/etc/dhcp/dhcpd.conf` : 

```
[leo@client1 ~]$ sudo cat /etc/dhcp/dhcpd.conf
default-lease-time 600;
max-lease-time 7200;
authoritative;

subnet 10.3.0.128 netmask 255.255.255.192 {
        range dynamic-bootp 10.3.0.131 10.3.0.189;
        option broadcast-address 10.3.0.191;
        option routers 10.3.0.190;
        option domain-name-servers 10.3.1.2, 1.1.1.1 ;
}
```

creation du dhcp : 

elle porte bien le bon nom : 
```
[leo@marcel ~]$ sudo cat /etc/hostname
marcel.client1.tp3
```

elle recupère une ip grace au DHCP : 
```
[leo@marcel ~]$ sudo cat /etc/sysconfig/network-scripts/ifcfg-enp0s8
TYPE=Ethernet
PROXY_METHOD=none
BROWSER_ONLY=no
BOOTPROTO=dhcp
DEFROUTE=yes
IPV4_FAILURE_FATAL=no
IPV6INIT=yes
IPV6_AUTOCONF=yes
IPV6_DEFROUTE=yes
IPV6_FAILURE_FATAL=no
NAME=enp0s8
UUID=a13f6a76-585c-4666-a794-ec37a7d1c920
DEVICE=enp0s8
ONBOOT=no
```

et il possède bien une ip dans le reseau de `client1` : 
```
[leo@marcel ~]$ ip a
2: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:84:13:99 brd ff:ff:ff:ff:ff:ff
    inet 10.3.0.131/26 brd 10.3.0.191 scope global dynamic noprefixroute enp0s8
       valid_lft 309sec preferred_lft 309sec
    inet6 fe80::a00:27ff:fe84:1399/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
```

il peut acceder à internet et il peut resoudre des noms de domaine : 

```
[leo@marcel ~]$ ping google.com
PING google.com (216.58.198.206) 56(84) bytes of data.
64 bytes from par10s27-in-f14.1e100.net (216.58.198.206): icmp_seq=1 ttl=113 time=25.7 ms
64 bytes from par10s27-in-f14.1e100.net (216.58.198.206): icmp_seq=2 ttl=113 time=23.10 ms
64 bytes from par10s27-in-f14.1e100.net (216.58.198.206): icmp_seq=3 ttl=113 time=25.0 ms
64 bytes from par10s27-in-f14.1e100.net (216.58.198.206): icmp_seq=4 ttl=113 time=27.8 ms
64 bytes from par10s27-in-f14.1e100.net (216.58.198.206): icmp_seq=5 ttl=113 time=23.4 ms
--- google.com ping statistics ---
5 packets transmitted, 5 received, 0% packet loss, time 8070ms
rtt min/avg/max/mdev = 23.425/25.181/27.830/1.544 ms
```


avec la commande `traceroute 8.8.8.8` on peut voir que il passe bien par le router :
```
[leo@marcel ~]$ sudo traceroute 8.8.8.8
traceroute to 8.8.8.8 (8.8.8.8), 30 hops max, 60 byte packets
 1  _gateway (10.3.0.190)  0.813 ms  0.775 ms  0.763 ms
 2  10.0.2.2 (10.0.2.2)  0.591 ms  0.561 ms  0.631 ms

```

### 2. Serveur DNS
#### A. Our own DNS server
#### B. SETUP copain


il porte bien le bon nom : 
```
[leo@server1 ~]$ echo 'dns1.server1.tp3' | sudo tee /etc/hostname
dns1.server1.tp3
```

il a bien une ip dans le meme reseau
```
2: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:aa:b8:1e brd ff:ff:ff:ff:ff:ff
    inet 10.3.0.2/25 brd 10.3.0.127 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:feaa:b81e/64 scope link
       valid_lft forever preferred_lft forever
```

installation de bind :
```
dnf install -y bind bind-utils
```

modification du fichier `named.conf` :
```
[leo@dns1 ~]$ sudo cat /etc/named.conf
options {
        listen-on port 53 { any; };
        listen-on-v6 port 53 { ::1; };
        directory       "/var/named";
        dump-file       "/var/named/data/cache_dump.db";
        statistics-file "/var/named/data/named_stats.txt";
        memstatistics-file "/var/named/data/named_mem_stats.txt";
        secroots-file   "/var/named/data/named.secroots";
        recursing-file  "/var/named/data/named.recursing";

        recursion yes;
        allow-query     { 10.3.0.0/25; 10.3.0.128/26; 10.3.0.192/28; };

        dnssec-enable yes;
        dnssec-validation yes;

        managed-keys-directory "/var/named/dynamic";

        pid-file "/run/named/named.pid";
        session-keyfile "/run/named/session.key";

        include "/etc/crypto-policies/back-ends/bind.config";
};

logging {
        channel default_debug {
                file "data/named.run";
                severity dynamic;
        };
};

zone "server1.tp3" {
        type master;
        file "/etc/bind/zones/server1.tp3.forward";
        allow-query     { 10.3.0.0/25; 10.3.0.128/26; 10.3.0.192/28; };
};

zone "server2.tp3" {
        type master;
        file "/etc/bind/zones/server2.tp3.forward";
        allow-query     { 10.3.0.0/25; 10.3.0.128/26; 10.3.0.192/28; };
};

include "/etc/named.rfc1912.zones";
include "/etc/named.root.key";
```

modification fichier forward server 1 :
```
[leo@dns1 ~]$ sudo cat /etc/bind/zones/server1.tp3.forward
$TTL    86400
@       IN      SOA     dns1.server1.tp3. root.server1.tp3. (
                              2         ; Serial
                         604800         ; Refresh
                          86400         ; Retry
                        2419200         ; Expire
                         604800 )       ; Minimum

@       IN      NS      dns1.server1.tp3.
dns1    IN      A       10.3.0.2

router  IN      A       10.3.0.126
```

modification du fichier forward server 2 :
```
[leo@dns1 ~]$ sudo cat /etc/bind/zones/server2.tp3.forward
$TTL    86400
@       IN      SOA     dns1.server2.tp3. root.server2.tp3. (
                              3         ; Serial
                         604800         ; Refresh
                          86400         ; Retry
                        2419200         ; Expire
                         604800 )       ; Minimum;

@       IN      NS      dns1.server2.tp3.
dns1    IN      A       10.3.0.2

router  IN      A       10.3.0.206
```

restart + enable du service named : 
```
[leo@dns1 ~]$ sudo systemctl start named
[leo@dns1 ~]$ sudo systemctl enable named
Created symlink /etc/systemd/system/multi-user.target.wants/named.service → /usr/lib/systemd/system/named.service.
[leo@dns1 ~]$ sudo systemctl status named
● named.service - Berkeley Internet Name Domain (DNS)
   Loaded: loaded (/usr/lib/systemd/system/named.service; enabled; vendor p>
   Active: active (running) since Mon 2021-10-11 19:35:31 CEST; 31s ago
 Main PID: 10210 (named)
    Tasks: 4 (limit: 4956)
   Memory: 59.2M
   CGroup: /system.slice/named.service
           └─10210 /usr/sbin/named -u named -c /etc/named.conf

Oct 11 19:35:31 dns1.server1.tp3 named[10210]: network unreachable resolvin>
Oct 11 19:35:31 dns1.server1.tp3 named[10210]: network unreachable resolvin>
Oct 11 19:35:31 dns1.server1.tp3 named[10210]: network unreachable resolvin>
Oct 11 19:35:31 dns1.server1.tp3 named[10210]: network unreachable resolvin>
Oct 11 19:35:31 dns1.server1.tp3 named[10210]: network unreachable resolvin>
Oct 11 19:35:31 dns1.server1.tp3 named[10210]: network unreachable resolvin>
Oct 11 19:35:31 dns1.server1.tp3 named[10210]: network unreachable resolvin>
Oct 11 19:35:31 dns1.server1.tp3 named[10210]: network unreachable resolvin>
Oct 11 19:35:31 dns1.server1.tp3 named[10210]: managed-keys-zone: Key 20326>
Oct 11 19:35:31 dns1.server1.tp3 named[10210]: resolver priming query compl>
```

modification du firewall pour ajouter le service : 
```
[leo@dns1 ~]$ sudo firewall-cmd --add-service=dns --permanent
success
[leo@dns1 ~]$ sudo firewall-cmd --reload
success
```

test du dns depuis marcel : 
```
[leo@marcel ~]$ dig google.com @10.3.0.2

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> google.com @10.3.0.2
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 53186
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 4, ADDITIONAL: 9

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
; COOKIE: 2405fda44925f9c39a2f29a96164772711588c5e8d9a3af5 (good)
;; QUESTION SECTION:
;google.com.                    IN      A

;; ANSWER SECTION:
google.com.             300     IN      A       142.250.179.78

;; AUTHORITY SECTION:
google.com.             172800  IN      NS      ns2.google.com.
google.com.             172800  IN      NS      ns3.google.com.
google.com.             172800  IN      NS      ns4.google.com.
google.com.             172800  IN      NS      ns1.google.com.

;; ADDITIONAL SECTION:
ns2.google.com.         172800  IN      A       216.239.34.10
ns1.google.com.         172800  IN      A       216.239.32.10
ns3.google.com.         172800  IN      A       216.239.36.10
ns4.google.com.         172800  IN      A       216.239.38.10
ns2.google.com.         172800  IN      AAAA    2001:4860:4802:34::a
ns1.google.com.         172800  IN      AAAA    2001:4860:4802:32::a
ns3.google.com.         172800  IN      AAAA    2001:4860:4802:36::a
ns4.google.com.         172800  IN      AAAA    2001:4860:4802:38::a

;; Query time: 326 msec
;; SERVER: 10.3.0.2#53(10.3.0.2)
;; WHEN: Mon Oct 11 19:40:56 CEST 2021
;; MSG SIZE  rcvd: 331
```

test de la zone forward : 
```
[leo@marcel ~]$ dig dns1.server1.tp3 @10.3.0.2

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> dns1.server1.tp3 @10.3.0.2
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 42288
;; flags: qr aa rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 1, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
; COOKIE: a75bb5cae77004248cd06b03616477895ed9c9b96e613719 (good)
;; QUESTION SECTION:
;dns1.server1.tp3.              IN      A

;; ANSWER SECTION:
dns1.server1.tp3.       86400   IN      A       10.3.0.2

;; AUTHORITY SECTION:
server1.tp3.            86400   IN      NS      dns1.server1.tp3.

;; Query time: 0 msec
;; SERVER: 10.3.0.2#53(10.3.0.2)
;; WHEN: Mon Oct 11 19:42:34 CEST 2021
;; MSG SIZE  rcvd: 103
```

pour chaque requete on peut voir que c'est bien le dns qui s'en occupe grace a la ligne `;; SERVER: 10.3.0.2#53(10.3.0.2)` 
car `10.3.0.2` est l'IP de mon server DNS.

tableau d'adressage:

| Nom       | Adresse du réseau | Masque           | Nombre de clients possibles | Adresse passerelle | Adresse broadcast |
|-----------|-------------------|------------------|-----------------------------|--------------------|--------------------
| `client1` | `10.3.0.128`      | `255.255.255.192`|               62            | `10.3.0.190`       | `10.33.0.191`     |
| `server1` | `10.3.0.0`        | `255.255.255.128`|               126           | `10.3.0.126`       | `10.33.0.127`     |
| `server2` | `10.3.0.192`      | `255.255.255.192`|               14            | `10.3.0.206`       | `10.33.0.207`     |

tableau des machines :

| Nom machine         | Adresse IP `client1` | Adresse IP `server1` | Adresse IP `server2` | Adresse de passerelle |
|---------------------|----------------------|----------------------|----------------------|-----------------------|
| `router.tp3`        | `10.3.0.190/26`      | `10.3.0.126/25`      | `10.3.0.206/28`      |       Carte NAT       |
| `dhcp.client1.tp3`  | `10.3.0.131/25`      |           X          |           X          | `10.3.0.126`          |
| `marcel.client1.tp3`|    IP dynamiques     |           X          |           X          | `10.3.0.190`          |
| `dns1.server1.tp3`  |           X          | `10.3.0.2/25`        |           X          | `10.3.0.126`          |


### 3. Get deeper
#### A. DNS forwarder

Désolé je ne suis pas allé au bout de ce Tp, malgré ton aide j'ai eu beaucoup des difficultés, j'ai essayé d'avancer un maximum mais au lieu de bloquer indéfiniment j'ai préféré finir celui de Linux ou je me sens bien plus a
l'aise plutôt que de bloquer sur celui la pendant des jours et n'en finir aucun.

![](https://media.giphy.com/media/d7fTn7iSd2ivS/giphy.gif)
