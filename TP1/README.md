#    **TP1 - Réseau, Mise en jambes**
## I. Exploration locale en solo
### 1. Affichage d'informations sur la pile TCP/IP locale

#### obtenir les adresses MAC et wifi : 
```
commande : ipconfig /all 
```
                                
#### interface WIFI : 
```

Wireless LAN adapter Wi-Fi:

   Connection-specific DNS Suffix  . : auvence.co
   Description . . . . . . . . . . . : Killer(R) Wi-Fi 6 AX1650i 160MHz Wireless Network Adapter (201NGW)
   Physical Address. . . . . . . . . : CC-F9-E4-C5-A3-32
   DHCP Enabled. . . . . . . . . . . : Yes
   Autoconfiguration Enabled . . . . : Yes
   Link-local IPv6 Address . . . . . : fe80::3d70:fa01:aac5:31d1%20(Preferred)
   IPv4 Address. . . . . . . . . . . : 10.33.0.253(Preferred)
   Subnet Mask . . . . . . . . . . . : 255.255.252.0
   Lease Obtained. . . . . . . . . . : lundi 13 septembre 2021 08:51:16
   Lease Expires . . . . . . . . . . : lundi 13 septembre 2021 12:45:22
   Default Gateway . . . . . . . . . : 10.33.3.253
   DHCP Server . . . . . . . . . . . : 10.33.3.254
   DHCPv6 IAID . . . . . . . . . . . : 130873828
   DHCPv6 Client DUID. . . . . . . . : 00-01-00-01-26-BF-0B-91-08-97-98-A4-1C-77
   DNS Servers . . . . . . . . . . . : 10.33.10.2
                                       10.33.10.148
                                       10.33.10.155
   NetBIOS over Tcpip. . . . . . . . : Enabled
```
**nom :** *Wireless LAN adapter Wi-Fi*
**MAC :** *CC-F9-E4-C5-A3-32*           
**IP :** *10.33.0.253*
        
#### interface Ethernet : 
```
Ethernet adapter Connexion réseau Bluetooth:

   Media State . . . . . . . . . . . : Media disconnected
   Connection-specific DNS Suffix  . :
   Description . . . . . . . . . . . : Bluetooth Device (Personal Area Network)
   Physical Address. . . . . . . . . : CC-F9-E4-C5-A3-36
   DHCP Enabled. . . . . . . . . . . : Yes
   Autoconfiguration Enabled . . . . : Yes
```
**nom :** *Ethernet adapter Connexion réseau Bluetooth*
**MAC :** *CC-F9-E4-C5-A3-36*           
**IP :** *non connecté*

##

#### connaître l'adresse IP de la passerelle de la carte WiFi : 

```
commande : ipconfig /all
```

#### interface WIFI : 
```

Wireless LAN adapter Wi-Fi:

   Connection-specific DNS Suffix  . : auvence.co
   Description . . . . . . . . . . . : Killer(R) Wi-Fi 6 AX1650i 160MHz Wireless Network Adapter (201NGW)
   Physical Address. . . . . . . . . : CC-F9-E4-C5-A3-32
   DHCP Enabled. . . . . . . . . . . : Yes
   Autoconfiguration Enabled . . . . : Yes
   Link-local IPv6 Address . . . . . : fe80::3d70:fa01:aac5:31d1%20(Preferred)
   IPv4 Address. . . . . . . . . . . : 10.33.0.253(Preferred)
   Subnet Mask . . . . . . . . . . . : 255.255.252.0
   Lease Obtained. . . . . . . . . . : lundi 13 septembre 2021 08:51:16
   Lease Expires . . . . . . . . . . : lundi 13 septembre 2021 12:45:22
   Default Gateway . . . . . . . . . : 10.33.3.253
   DHCP Server . . . . . . . . . . . : 10.33.3.254
   DHCPv6 IAID . . . . . . . . . . . : 130873828
   DHCPv6 Client DUID. . . . . . . . : 00-01-00-01-26-BF-0B-91-08-97-98-A4-1C-77
   DNS Servers . . . . . . . . . . . : 10.33.10.2
                                       10.33.10.148
                                       10.33.10.155
   NetBIOS over Tcpip. . . . . . . . : Enabled
```
**Gateway carte WIFI :** *10.33.3.253*

##

#### Trouvez les informations sur une carte IP (GUI)

chemin sous Windows 10 :

```
Setting > Network & Internet > Status > View hardware and connection properties > 
```

resultat : 
![](https://i.imgur.com/x2xg68j.png)

**IP :** *10.33.0.253/22* - [cadre bleu]
**MAC :** *cc:f9:e4:c5:a3:32* - [cadre rouge]
**Gateway** *10.33.3.253* - [cadre vert]

#### Question :
##### à quoi sert la gateway dans le réseau d'YNOV ?

chez ynov la gateway sert a connecté le réseau de l'école aux autres réseaux exterieurs 

##

### 2. Modifications des informations : 
#### A. Modification d'adresse IP (part 1) :

#### Utilisation de l'interface graphique pour changer d'IP 

chemin sous Windows 10 :

```
Setting > Network & Internet > WIFI > Actual Wifi (WIFI@YNOV)> IP Settings > EDIT 
```

resultat : 
![](https://i.imgur.com/dtO8T3f.png)

#### Question :
##### Expliquez pourquoi on perd l'acces a internet :

Le serveur associe une adresse IP et une adresse MAC, si on change note adresse IP, le serveur nous reconnait grace a notre adresse MAC, mais comme il y a un serveur DHCP, il n'accepte pas que nous changions d'IP.

#### B. La table ARP :
##### Affichage de la table ARP :

```
commande : arp -a
```

resultat : 
```
Interface: 192.168.61.1 --- 0x7
  Internet Address      Physical Address      Type
  192.168.61.255        ff-ff-ff-ff-ff-ff     static
  224.0.0.22            01-00-5e-00-00-16     static
  224.0.0.251           01-00-5e-00-00-fb     static
  224.0.0.252           01-00-5e-00-00-fc     static
  239.255.255.250       01-00-5e-7f-ff-fa     static
  255.255.255.255       ff-ff-ff-ff-ff-ff     static

Interface: 169.254.232.131 --- 0xd
  Internet Address      Physical Address      Type
  169.254.255.255       ff-ff-ff-ff-ff-ff     static
  224.0.0.22            01-00-5e-00-00-16     static
  224.0.0.251           01-00-5e-00-00-fb     static
  224.0.0.252           01-00-5e-00-00-fc     static
  230.0.0.1             01-00-5e-00-00-01     static
  239.255.255.250       01-00-5e-7f-ff-fa     static
  255.255.255.255       ff-ff-ff-ff-ff-ff     static

Interface: 169.254.245.190 --- 0x13
  Internet Address      Physical Address      Type
  169.254.255.255       ff-ff-ff-ff-ff-ff     static
  224.0.0.22            01-00-5e-00-00-16     static
  224.0.0.251           01-00-5e-00-00-fb     static
  224.0.0.252           01-00-5e-00-00-fc     static
  239.255.255.250       01-00-5e-7f-ff-fa     static
  255.255.255.255       ff-ff-ff-ff-ff-ff     static

Interface: 10.33.0.253 --- 0x15
  Internet Address      Physical Address      Type
  10.33.3.253           00-12-00-40-4c-bf     dynamic
  10.33.3.255           ff-ff-ff-ff-ff-ff     static
  224.0.0.22            01-00-5e-00-00-16     static
  224.0.0.251           01-00-5e-00-00-fb     static
  224.0.0.252           01-00-5e-00-00-fc     static
  239.255.255.250       01-00-5e-7f-ff-fa     static
  255.255.255.255       ff-ff-ff-ff-ff-ff     static
```

Identification de l'adresse mac de ma passerelle :

l'adresse IP de ma passerelle est : `10.33.3.253`

dans ma tables ARP la ligne qui corespond a cette IP est : 
`10.33.3.253           00-12-00-40-4c-bf     dynamic`

on peut donc voir que l'adresse MAC associée a cette IP est : 
`00-12-00-40-4c-bf

ping des IP : `10.33.3.52`,`10.33.2.8`,`10.33.3.10`

la nouvelle table ARP :
```
Interface: 192.168.61.1 --- 0x7
  Internet Address      Physical Address      Type
  192.168.61.254        00-50-56-e1-48-c1     dynamic
  192.168.61.255        ff-ff-ff-ff-ff-ff     static
  224.0.0.22            01-00-5e-00-00-16     static
  224.0.0.251           01-00-5e-00-00-fb     static
  224.0.0.252           01-00-5e-00-00-fc     static
  239.255.255.250       01-00-5e-7f-ff-fa     static
  255.255.255.255       ff-ff-ff-ff-ff-ff     static

Interface: 169.254.232.131 --- 0xd
  Internet Address      Physical Address      Type
  169.254.255.255       ff-ff-ff-ff-ff-ff     static
  224.0.0.22            01-00-5e-00-00-16     static
  224.0.0.251           01-00-5e-00-00-fb     static
  224.0.0.252           01-00-5e-00-00-fc     static
  230.0.0.1             01-00-5e-00-00-01     static
  239.255.255.250       01-00-5e-7f-ff-fa     static
  255.255.255.255       ff-ff-ff-ff-ff-ff     static

Interface: 169.254.245.190 --- 0x13
  Internet Address      Physical Address      Type
  169.254.255.255       ff-ff-ff-ff-ff-ff     static
  224.0.0.22            01-00-5e-00-00-16     static
  224.0.0.251           01-00-5e-00-00-fb     static
  224.0.0.252           01-00-5e-00-00-fc     static
  239.255.255.250       01-00-5e-7f-ff-fa     static
  255.255.255.255       ff-ff-ff-ff-ff-ff     static

Interface: 10.33.0.253 --- 0x15
  Internet Address      Physical Address      Type
  10.33.0.46            9c-fc-e8-36-7b-ee     dynamic
  10.33.0.100           e8-6f-38-6a-b6-ef     dynamic
  10.33.0.119           18-56-80-70-9c-48     dynamic
  10.33.0.251           48-e7-da-41-bf-e1     dynamic
  10.33.1.10            b0-7d-64-b1-98-d3     dynamic
  10.33.1.243           34-7d-f6-5a-20-da     dynamic
  10.33.2.8             a4-5e-60-ed-0b-27     dynamic
  10.33.2.56            c0-3c-59-a9-e0-75     dynamic
  10.33.2.81            50-eb-71-d6-4e-8f     dynamic
  10.33.2.188           d8-f3-bc-c2-f5-39     dynamic
  10.33.3.10            a4-83-e7-59-ae-c2     dynamic
  10.33.3.52            d8-f2-ca-0e-a8-44     dynamic
  10.33.3.253           00-12-00-40-4c-bf     dynamic
  10.33.3.254           00-0e-c4-cd-74-f5     dynamic
  10.33.3.255           ff-ff-ff-ff-ff-ff     static
  224.0.0.22            01-00-5e-00-00-16     static
  224.0.0.251           01-00-5e-00-00-fb     static
  224.0.0.252           01-00-5e-00-00-fc     static
  239.255.255.250       01-00-5e-7f-ff-fa     static
  255.255.255.255       ff-ff-ff-ff-ff-ff     static
```

les adresses MAC associées aux adresses IP ping sont :

```
10.33.3.52            d8-f2-ca-0e-a8-44     dynamic
10.33.2.8             a4-5e-60-ed-0b-27     dynamic
10.33.3.10            a4-83-e7-59-ae-c2     dynamic
```

scan des adresses IP d'ynov avec nmap : 

commande : `nmap -sP 10.33.0.0/22 -v`

resultat : ![](https://i.imgur.com/e2uefHl.gif)

il y beacoup d'adresses donc voici un extrait : 

sur les 1024 adresses scannées on peut voir que certaines sont UP et d'autres DOWN : 

exemple d'une adresse UP :
`Nmap scan report for 10.33.0.7
Host is up (0.23s latency).
MAC Address: 9C:BC:F0:B6:1B:ED (Xiaomi Communications)
`

exemple d'une adresse DOWN : 
`
Nmap scan report for 10.33.0.8 [host down]
`

scan du réseau d'Ynov pour trouver une adresse libre :
commande : `nmap -sP 10.33.0.0/22 -v`

avec la commande `nmap -sP 10.33.0.0/22 -v` il suffit de choisir une adresse qui est indiqué avec *DOWN* comme : `10.33.3.235`


puis je definis ma nouvelle IP en rentrant les paramètres suivants : 
![](https://i.imgur.com/aLhWkie.png)


avec `ipconfig;ping 1.1.1.1` , on peut voir que j'ai bien l'ip choisis et que je peux bien ping un serveur dns :
```
Wireless LAN adapter Wi-Fi:

   Connection-specific DNS Suffix  . :
   Link-local IPv6 Address . . . . . : fe80::3d70:fa01:aac5:31d1%21
   IPv4 Address. . . . . . . . . . . : 10.33.3.235
   Subnet Mask . . . . . . . . . . . : 255.255.252.0
   Default Gateway . . . . . . . . . : 10.33.3.253

Pinging 1.1.1.1 with 32 bytes of data:
Reply from 1.1.1.1: bytes=32 time=83ms TTL=58
Reply from 1.1.1.1: bytes=32 time=34ms TTL=58
Reply from 1.1.1.1: bytes=32 time=23ms TTL=58
Reply from 1.1.1.1: bytes=32 time=19ms TTL=58

Ping statistics for 1.1.1.1:
    Packets: Sent = 4, Received = 4, Lost = 0 (0% loss),
Approximate round trip times in milli-seconds:
    Minimum = 19ms, Maximum = 83ms, Average = 39ms
```

## II. Exploration locale en duo

vérification du changement d'ip avec `ipconfig`

```
Ethernet adapter Ethernet:

   Connection-specific DNS Suffix  . :
   Link-local IPv6 Address . . . . . : fe80::68d3:fcdc:a589:b0%10
   IPv4 Address. . . . . . . . . . . : 192.168.1.1
   Subnet Mask . . . . . . . . . . . : 255.255.255.252
   Default Gateway . . . . . . . . . : 192.168.1.2
```

mon IP est bien : `192.168.1.1`

Ping du deuxieme PC avec la commande `ping`

resultat : 
```
Pinging 192.168.1.2 with 32 bytes of data:
Reply from 192.168.1.2: bytes=32 time=1ms TTL=64
Reply from 192.168.1.2: bytes=32 time=1ms TTL=64
Reply from 192.168.1.2: bytes=32 time=1ms TTL=64
Reply from 192.168.1.2: bytes=32 time=1ms TTL=64

Ping statistics for 192.168.1.2:
    Packets: Sent = 4, Received = 4, Lost = 0 (0% loss),
Approximate round trip times in milli-seconds:
    Minimum = 1ms, Maximum = 1ms, Average = 1ms
```

si on regarde dans la table ARP avec la commande `arp -a` on peut voir ceci :

```
arp -a

Interface: 192.168.61.1 --- 0x7
  Internet Address      Physical Address      Type
  192.168.61.254        00-50-56-e1-48-c1     dynamic
  192.168.61.255        ff-ff-ff-ff-ff-ff     static
  224.0.0.22            01-00-5e-00-00-16     static
  224.0.0.251           01-00-5e-00-00-fb     static
  224.0.0.252           01-00-5e-00-00-fc     static
  239.255.255.250       01-00-5e-7f-ff-fa     static
  255.255.255.255       ff-ff-ff-ff-ff-ff     static

Interface: 192.168.1.1 --- 0xa
  Internet Address      Physical Address      Type
  192.168.1.2           b4-2e-99-f6-a6-43     dynamic
  192.168.1.3           ff-ff-ff-ff-ff-ff     static
  224.0.0.22            01-00-5e-00-00-16     static
  224.0.0.251           01-00-5e-00-00-fb     static
  224.0.0.252           01-00-5e-00-00-fc     static
  239.255.255.250       01-00-5e-7f-ff-fa     static
  255.255.255.255       ff-ff-ff-ff-ff-ff     static

Interface: 169.254.232.131 --- 0xd
  Internet Address      Physical Address      Type
  169.254.255.255       ff-ff-ff-ff-ff-ff     static
  224.0.0.22            01-00-5e-00-00-16     static
  224.0.0.251           01-00-5e-00-00-fb     static
  224.0.0.252           01-00-5e-00-00-fc     static
  239.255.255.250       01-00-5e-7f-ff-fa     static
  255.255.255.255       ff-ff-ff-ff-ff-ff     static

Interface: 169.254.245.190 --- 0x13
  Internet Address      Physical Address      Type
  169.254.255.255       ff-ff-ff-ff-ff-ff     static
  224.0.0.22            01-00-5e-00-00-16     static
  224.0.0.251           01-00-5e-00-00-fb     static
  224.0.0.252           01-00-5e-00-00-fc     static
  239.255.255.250       01-00-5e-7f-ff-fa     static
  255.255.255.255       ff-ff-ff-ff-ff-ff     static
```

on peut bien voir l'IP du deuxieme PC à la ligne : 

`192.168.1.2           b4-2e-99-f6-a6-43     dynamic`

ensuite on test la connexion a internet avec un ping d'un DNS :

```
ping 1.1.1.1

Pinging 1.1.1.1 with 32 bytes of data:
Reply from 1.1.1.1: bytes=32 time=18ms TTL=57
Reply from 1.1.1.1: bytes=32 time=18ms TTL=57
Reply from 1.1.1.1: bytes=32 time=17ms TTL=57
Reply from 1.1.1.1: bytes=32 time=20ms TTL=57

Ping statistics for 1.1.1.1:
    Packets: Sent = 4, Received = 4, Lost = 0 (0% loss),
Approximate round trip times in milli-seconds:
    Minimum = 17ms, Maximum = 20ms, Average = 18ms
```

et enfin on utilise la commande `tracert 1.1.1.1` pour voir par ou passe les requetes : 

résultat : 
```
tracert 1.1.1.1

Tracing route to one.one.one.one [1.1.1.1]
over a maximum of 30 hops:

  1     1 ms    <1 ms    <1 ms  DESKTOP-DG9JKP6 [192.168.1.2]
  2     *        *        *     Request timed out.
  3     3 ms     3 ms     2 ms  10.33.3.253
  4    35 ms     5 ms     4 ms  10.33.10.254
  5     2 ms     2 ms     2 ms  reverse.completel.net [92.103.174.137]
  6     8 ms     8 ms     8 ms  92.103.120.182
  7    23 ms    22 ms    23 ms  172.19.130.117
  8    30 ms    30 ms    32 ms  46.218.128.74
  9    26 ms    29 ms    18 ms  equinix-paris.cloudflare.com [195.42.144.143]
 10    17 ms    17 ms    17 ms  one.one.one.one [1.1.1.1]

Trace complete.
```

### 5. Petit chat privé :

sur le pc serveur on entre la commande suivante : 

`nc.exe -l -p 8888`

et sur le pc client on entre la commande : 

`nc.exe 192.168.1.1 8888`

ensuite on peut utiliser le chat : 
![](https://i.imgur.com/8BMaTG5.png)


ensuite pour aller plus loin on peut utiliser la commande : 
`nc.exe -l -p 9999 192.168.1.2`

```
C:\Users\Leo\Desktop>nc.exe -l -p 9999 192.168.1.2
        rffsdf
        ffsdf
        df
        fsd
        fsdf
        sdf
```

on peut aussi accepter uniquement les connexions internes à la machine en écoutant sur 127.0.0.1

mais nous obtenons cette erreur sur le PC serveur : 

```
 C:\Users\mattf\Desktop>nc.exe -l -p 9999 192.168.1.37
        invalid connection to [192.168.1.2] from (UNKNOWN)[192.168.1.1] 21434
```

### 6. Firewall

configuration du firewall pour autoriser les ping :

commande : `netsh advfirewall firewall add rule name="ICMP Allow incoming V4 echo request" protocol=icmpv4:8,any dir=in action=allow`

puis on test un ping vers l'autre pc avec la commande :
`ping 192.168.1.2`

et le resultat est : 
```
Pinging 192.168.1.2 with 32 bytes of data:
Reply from 192.168.1.2: bytes=32 time=1ms TTL=64
Reply from 192.168.1.2: bytes=32 time<1ms TTL=64
Reply from 192.168.1.2: bytes=32 time=1ms TTL=64
Reply from 192.168.1.2: bytes=32 time=1ms TTL=64

Ping statistics for 192.168.1.2:
    Packets: Sent = 4, Received = 4, Lost = 0 (0% loss),
Approximate round trip times in milli-seconds:
    Minimum = 0ms, Maximum = 1ms, Average = 0ms
```

ensuite on autorise NC a utilisé un port spécifique :
commande : `netsh advfirewall firewall add rule name= "Open Port 80" dir=in action=allow protocol=TCP localport=15806`


résultat du chat : 
>nc.exe 192.168.1.2 15801
f
fsdf
dsf
sdf


## Manipulations d'autres outils/protocoles côté client
### 1. DHCP

je n'etais pas a ynov donc j'ai utilisé mon propre réseau :

j'utilise la commande : `ipconfig /all` 

l'ip de mon DHCP est : 192.168.1.254
la date d'expiration est : lundi 20 septembre 2021 08:02:44

## 2. DNS

j'utilise la commande : `ipconfig /all` 

l'ip de mon DNS est : 192.168.1.254

j'utilise la commande : `nslookup [website]`


pour google.com : 


```
nslookup google.com
Server:  UnKnown
Address:  192.168.1.254

Non-authoritative answer:
Name:    google.com
Addresses:  2a00:1450:4007:819::200e
          142.250.179.78
```
          
          
pour ynov.com :
```

 nslookup ynov.com
Server:  UnKnown
Address:  192.168.1.254

Non-authoritative answer:
Name:    ynov.com
Address:  92.243.16.143
```


## IV. Wireshark
nous ecoutons le reseau avec wireshark et nous obtenons :

```
228	1.370964	192.168.1.69	192.168.1.254	DNS	70	Standard query 0x3f21 A google.com
229	1.381863	192.168.1.254	192.168.1.69	DNS	86	Standard query response 0x3f21 A google.com A 216.58.214.78

230	1.386144	2a01:e0a:18b:e620:98d5:7995:d0b3:3ebd	2a00:1450:4007:816::200e	ICMPv6	94	Echo (ping) request id=0x0001, seq=8, hop limit=128 (reply in 233)
233	1.396553	2a00:1450:4007:816::200e	2a01:e0a:18b:e620:98d5:7995:d0b3:3ebd	ICMPv6	94	Echo (ping) reply id=0x0001, seq=8, hop limit=118 (request in 230)

372	2.388794	2a01:e0a:18b:e620:98d5:7995:d0b3:3ebd	2a00:1450:4007:816::200e	ICMPv6	94	Echo (ping) request id=0x0001, seq=9, hop limit=128 (reply in 374)
374	2.399367	2a00:1450:4007:816::200e	2a01:e0a:18b:e620:98d5:7995:d0b3:3ebd	ICMPv6	94	Echo (ping) reply id=0x0001, seq=9, hop limit=118 (request in 372)

485	3.392334	2a01:e0a:18b:e620:98d5:7995:d0b3:3ebd	2a00:1450:4007:816::200e	ICMPv6	94	Echo (ping) request id=0x0001, seq=10, hop limit=128 (reply in 488)
488	3.403452	2a00:1450:4007:816::200e	2a01:e0a:18b:e620:98d5:7995:d0b3:3ebd	ICMPv6	94	Echo (ping) reply id=0x0001, seq=10, hop limit=118 (request in 485)

651	4.395539	2a01:e0a:18b:e620:98d5:7995:d0b3:3ebd	2a00:1450:4007:816::200e	ICMPv6	94	Echo (ping) request id=0x0001, seq=11, hop limit=128 (reply in 652)
652	4.407774	2a00:1450:4007:816::200e	2a01:e0a:18b:e620:98d5:7995:d0b3:3ebd	ICMPv6	94	Echo (ping) reply id=0x0001, seq=11, hop limit=118 (request in 651)
```

nous pouvons voir que les premières lignes sont pour la requetes du DNS et ensuite 4 fois 2 lignes qui corespondent au 4 pings (envoi et reception) a chaque fois.

