# TP4 : Vers un réseau d'entreprise

## I. Dumb switch
## 2. Adressage topologie 1

| Node  | IP            |
|-------|---------------|
| `pc1` | `10.1.1.1/24` |
| `pc2` | `10.1.1.2/24` |

### 3. Setup topologie 1

on definis 2 ip pour les vpcs :

pour le pc 1 : 
```
PC1> ip 10.1.1.1/24
Checking for duplicate address...
PC1 : 10.1.1.1 255.255.255.0
```

pour le pc 2 : 
```
PC2> ip 10.1.1.2
Checking for duplicate address...
PC2 : 10.1.1.2 255.255.255.0
```

puis on effectue un ping entre les deux vpcs :
```
PC1> ping 10.1.1.2

84 bytes from 10.1.1.2 icmp_seq=1 ttl=64 time=9.740 ms
84 bytes from 10.1.1.2 icmp_seq=2 ttl=64 time=6.356 ms
84 bytes from 10.1.1.2 icmp_seq=3 ttl=64 time=6.823 ms
84 bytes from 10.1.1.2 icmp_seq=4 ttl=64 time=5.094 ms
```

## II. VLAN
### 2. Adressage topologie 1

| Node  | IP            | VLAN |
|-------|---------------|------|
| `pc1` | `10.1.1.1/24` | 10   |
| `pc2` | `10.1.1.2/24` | 10   |
| `pc3` | `10.1.1.3/24` | 20   |

### 3. Setup topologie 2

je definis une ip pour la machine n°3 :
```
PC3> ip 10.1.1.3
Checking for duplicate address...
PC3 : 10.1.1.3 255.255.255.0
```

elles peuvent se ping :
```
PC3> ping 10.1.1.1

84 bytes from 10.1.1.1 icmp_seq=1 ttl=64 time=6.390 ms
84 bytes from 10.1.1.1 icmp_seq=2 ttl=64 time=8.000 ms

PC3> ping 10.1.1.2

84 bytes from 10.1.1.2 icmp_seq=1 ttl=64 time=6.979 ms
84 bytes from 10.1.1.2 icmp_seq=2 ttl=64 time=8.116 ms
``` 


Configuration des VLANs : 
```
Switch(config)#vlan 10
Switch(config-vlan)#
Switch(config-vlan)#name admins
Switch(config-vlan)#
Switch(config-vlan)#exit


Switch(config)#vlan 20
Switch(config-vlan)#
Switch(config-vlan)#name guests
Switch(config-vlan)#
Switch(config-vlan)#exit
```

atribution d'un trunk pour les PC 1,2, et 3 :

pc 1 : 
```
Switch(config-if)#interface Gi0/0
Switch(config-if)#
Switch(config-if)#switchport mode access
Switch(config-if)#
Switch(config-if)#switchport access vlan 10
Switch(config-if)#
Switch(config-if)#exit
```


pc 2 : 
```
Switch(config)#interface Gi0/1
Switch(config-if)#
Switch(config-if)#switchport mode access
Switch(config-if)#
Switch(config-if)#switchport access vlan 10
Switch(config-if)#
Switch(config-if)#exit
```

pc 3 : 
````
Switch(config)#interface Gi0/2
Switch(config-if)#
Switch(config-if)#switchport mode access
Switch(config-if)#
Switch(config-if)#switchport access vlan 20
Switch(config-if)#
Switch(config-if)#exit
````

on vérifie avec des ping :

ping pc 1 vers pc 2 : 
```
PC1> ping 10.1.1.2

84 bytes from 10.1.1.2 icmp_seq=1 ttl=64 time=6.001 ms
84 bytes from 10.1.1.2 icmp_seq=2 ttl=64 time=2.592 ms
84 bytes from 10.1.1.2 icmp_seq=3 ttl=64 time=3.502 ms
84 bytes from 10.1.1.2 icmp_seq=4 ttl=64 time=1.783 ms
```

ping pc 3 vers pc 1 :
```
PC3> ping 10.1.1.1

host (10.1.1.1) not reachable
```

ping pc 2 vers pc 2 :
```
PC3> ping 10.1.1.2

host (10.1.1.2) not reachable
```

## III. Routing
### 2. Adressage topologie 3

Les réseaux et leurs VLANs associés :

| Réseau    | Adresse       | VLAN associé |
|-----------|---------------|--------------|
| `clients` | `10.1.1.0/24` | 11           |
| `admins`  | `10.2.2.0/24` | 12           |
| `servers` | `10.3.3.0/24` | 13           |

L'adresse des machines au sein de ces réseaux :

| Node               | `clients`       | `admins`        | `servers`       |
|--------------------|-----------------|-----------------|-----------------|
| `pc1.clients.tp4`  | `10.1.1.1/24`   | x               | x               |
| `pc2.clients.tp4`  | `10.1.1.2/24`   | x               | x               |
| `adm1.admins.tp4`  | x               | `10.2.2.1/24`   | x               |
| `web1.servers.tp4` | x               | x               | `10.3.3.1/24`   |
| `r1`               | `10.1.1.254/24` | `10.2.2.254/24` | `10.3.3.254/24` |


### 3. Setup topologie 3

creation + checklist de `web1.servers.tp4`

creation de 3 Vlans (11 , 12 ,13) :
```
Switch(config)#vlan 11
Switch(config-vlan)#
Switch(config-vlan)#name clients
Switch(config-vlan)#
Switch(config-vlan)#exit

Switch(config)#vlan 12
Switch(config-vlan)#
Switch(config-vlan)#name admins
Switch(config-vlan)#
Switch(config-vlan)#exit

Switch(config)#vlan 13
Switch(config-vlan)#
Switch(config-vlan)#name servers
Switch(config-vlan)#
```

atribution des ports au bon vlans :
```
Switch#show vlan br

VLAN Name                             Status    Ports
---- -------------------------------- --------- -------------------------------
1    default                          active    Gi0/3, Gi1/1, Gi1/2, Gi1/3
                                                Gi2/0, Gi2/1, Gi2/2, Gi2/3
                                                Gi3/0, Gi3/1, Gi3/2, Gi3/3
11   clients                          active    Gi0/0, Gi0/1
12   admins                           active    Gi0/2
13   servers                          active    Gi1/0
```

ajout d'un port trunk entre le switch et le routeur :
```
Switch(config-if)#interface Gi0/3
Switch(config-if)#
Switch(config-if)#switchport trunk encapsulation dot1q
Switch(config-if)#
Switch(config-if)#switchport mode trunk
Switch(config-if)#
Switch(config-if)#switchport trunk allowed vlan add 11,12,13
Switch(config-if)#
Switch(config-if)#exit
Switch(config)#
Switch(config)#exit
Switch#

Switch#show interface trunk

Port        Mode             Encapsulation  Status        Native vlan
Gi0/3       on               802.1q         trunking      1

Port        Vlans allowed on trunk
Gi0/3       1-4094

Port        Vlans allowed and active in management domain
Gi0/3       1,11-13

Port        Vlans in spanning tree forwarding state and not pruned
Gi0/3       1,11-13
```
 
 
j'ajoute plusieurs ip a mon routeur : 
```
R1(config)#interface fastEthernet 0/0.11
R1(config-subif)#encapsulation dot1Q 11
R1(config-subif)#ip addr 10.1.1.254 255.255.255.0
R1(config-subif)#exit
R1(config)#interface fastEthernet 0/0.12
R1(config-subif)#encapsulation dot1Q 12
R1(config-subif)#ip addr 10.2.2.254 255.255.255.0
R1(config-subif)#exit
R1(config)#interface fastEthernet 0/0.13
R1(config-subif)#encapsulation dot1Q 13
R1(config-subif)#ip addr 10.3.3.254 255.255.255.0
R1(config-subif)#exit
```

```
R1#show ip int br
Interface                  IP-Address      OK? Method Status                Protocol
FastEthernet0/0            unassigned      YES NVRAM  up                    up
FastEthernet0/0.11         10.1.1.254      YES manual up                    up
FastEthernet0/0.12         10.2.2.254      YES manual up                    up
FastEthernet0/0.13         10.3.3.254      YES manual up                    up
FastEthernet1/0            unassigned      YES NVRAM  administratively down down
FastEthernet2/0            unassigned      YES NVRAM  administratively down down
FastEthernet3/0            unassigned      YES NVRAM  administratively down down
```

on fait les vérifications :
```
PC1> ping 10.1.1.254

84 bytes from 10.1.1.254 icmp_seq=1 ttl=255 time=30.193 ms
84 bytes from 10.1.1.254 icmp_seq=2 ttl=255 time=24.519 ms
84 bytes from 10.1.1.254 icmp_seq=3 ttl=255 time=15.305 ms
84 bytes from 10.1.1.254 icmp_seq=4 ttl=255 time=17.081 ms
84 bytes from 10.1.1.254 icmp_seq=5 ttl=255 time=19.268 ms

PC2> ping 10.1.1.254

84 bytes from 10.1.1.254 icmp_seq=1 ttl=255 time=7.095 ms
84 bytes from 10.1.1.254 icmp_seq=2 ttl=255 time=9.812 ms
84 bytes from 10.1.1.254 icmp_seq=3 ttl=255 time=9.438 ms
84 bytes from 10.1.1.254 icmp_seq=4 ttl=255 time=18.798 ms

PC3> ping 10.2.2.254

84 bytes from 10.2.2.254 icmp_seq=1 ttl=255 time=19.159 ms
84 bytes from 10.2.2.254 icmp_seq=2 ttl=255 time=7.434 ms
84 bytes from 10.2.2.254 icmp_seq=3 ttl=255 time=14.508 ms
84 bytes from 10.2.2.254 icmp_seq=4 ttl=255 time=11.164 ms


[leo@web1 ~]$ ping 10.3.3.254
PING 10.3.3.254 (10.3.3.254) 56(84) bytes of data.
64 bytes from 10.3.3.254: icmp_seq=1 ttl=255 time=24.5 ms
64 bytes from 10.3.3.254: icmp_seq=2 ttl=255 time=11.2 ms

```

ajout des routes par default dans les vpcs : 
```
PC1> ip 10.1.1.1/24 10.1.1.254
Checking for duplicate address...
PC1 : 10.1.1.1 255.255.255.0 gateway 10.1.1.254

PC2> ip 10.1.1.2/24 10.1.1.254
Checking for duplicate address...
PC2 : 10.1.1.2 255.255.255.0 gateway 10.1.1.254

PC3> ip 10.2.2.1/24 10.2.2.254
Checking for duplicate address...
PC3 : 10.2.2.1 255.255.255.0 gateway 10.2.2.254
```

ajout d'un route par default dans la vm dans `/etc/sysconfig/network-scripts/ifcfg-enp0s8` :
```
GATEWAY=10.3.3.254
```

ping entre les réseaux :
```
PC1> ping 10.3.3.1
84 bytes from 10.3.3.1 icmp_seq=1 ttl=63 time=34.128 ms
84 bytes from 10.3.3.1 icmp_seq=2 ttl=63 time=29.603 ms

PC3> ping 10.1.1.1
84 bytes from 10.1.1.1 icmp_seq=1 ttl=63 time=11.425 ms
84 bytes from 10.1.1.1 icmp_seq=2 ttl=63 time=39.274 ms

[leo@web1 ~]$ ping 10.2.2.1
PING 10.2.2.1 (10.2.2.1) 56(84) bytes of data.
64 bytes from 10.2.2.1: icmp_seq=1 ttl=63 time=25.6 ms
64 bytes from 10.2.2.1: icmp_seq=2 ttl=63 time=31.4 ms
```
